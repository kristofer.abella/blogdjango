from django.views.generic import View
from django.shortcuts import render
from .models import Post

class BlogView(View):
    def get(self, request):
        post=Post.objects.all()
        context = {'posts':post}
        return render(request, 'blog.html',context)