
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static #para agregar ruta donde se alojaran los recursos media
from django.conf import settings
from .views import HomeView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',HomeView.as_view(),name='home'),
    path('blog/',include('blog.urls'),name='blog'),
]

urlpatterns+=static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)